configure :development do
  set :asset_host, "//#{ENV['FOG_DIRECTORY']}.s3.amazonaws.com/assets/images"
end