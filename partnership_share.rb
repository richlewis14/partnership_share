require 'bundler/setup'
Bundler.require(:default)
require 'rubygems'
require 'sinatra'
require 'active_support/all'
require 'sinatra/flash'
require 'sinatra/static_assets'
require 'pony'
require './config/config_aws.rb' if File.exists?('./config/config_aws.rb')
require './helpers/aws_helper'
require './config/environments/development'
require './config/environments/production'



enable :sessions
set :static_cache_control, [:public, :max_age => 31536000]

get '/' do 
  erb :index
end

get '/about_us' do
  erb :about_us
end

get '/domiciliary_care' do
  erb :domiciliary_care
end

get '/supported_living_services' do
  erb :supported_living_services
end

get '/person_centered_planning' do
  erb :person_centered_planning
end

get '/our_team' do
  erb :our_team
end

get '/contact' do
  erb :contact
end

post '/contact' do
    from = params[:name]
    subject = "#{params[:name]} has contacted you from the Partnership Share Website"
    body = erb(:mail, layout: false)

  Pony.mail(
  :from => from,
  :to => ENV["EMAIL_ADDRESS"],
  :subject => subject,
  :body => body,
  :via => :smtp,
  :via_options => {
    :address              => 'smtp.gmail.com',
    :port                 => '587',
    :enable_starttls_auto => true,
    :user_name            => ENV["USER_NAME"],
    :password             => ENV["PASSWORD"],
    :authentication       => :plain, 
    :domain               => "localhost.localdomain" 
})
  flash[:notice] = "Thanks for your email. We will be in touch shortly."
  redirect '/success' 

end

get '/success' do
  erb :index
end
	


