//-----------------------------Scroll to top-----------------//
$(document).ready(function(){ 
  $(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
      $('.scrollup').fadeIn();
    } else {
      $('.scrollup').fadeOut();
    }
  }); 
 
    $('.scrollup').click(function(){
      $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
      });

//-----------------------------Fadeout Flash Notice-----------------//
     $('#flash').fadeOut(4000, function () {  
      }); 

    });
